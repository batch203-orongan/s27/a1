// Activity s27

// 1. Create sample documents following the models we have created for users and courses for our booking-system.

	// - user1 and user2 are the ids for the user documents.
	// - course1 and course2 are the ids for the course documents.

	// - user1 is enrolled in course1.
	// - user2 is enrolled in course2.

// 2 Model Booking System with Embedding

user {

	"id": "user1",
	"username": "user123",
	"firstName":"Juan",
	"lastName":"Cruz",
	"email":"juancruz@gmail.com",
	"password":"jc1234",
	"mobileNumber":"09123456789",
	"isAdmin": false,
	"enrollments": [
		{

			"id": "enrollee01",
			"courseId":"course1",
			"courseName": "Math101",
			"isPaid": true,
			"dateEnrolled": "08/25/2022:4:00 pm"
		}
	]

}


course {

	"id": "course1"
	"name":"Math101",
	"description": "Trigonometry",
	"price":"10000",
	"slots":"20",
	"schedule":"7:00 -10:00 am",
	"instructor": "Mr. Saitama",
	"isActive":true,
	"enrollees": [

		{
			"id": "enrolee01",
			"userId":"user1",
			"userName": "user123",
			"isPaid": true,
			"dateEnrolled": "08/25/2022:4:00 pm"
		}

	]

}




//3 Model Booking System with Referencing

user {

	"id":"user2",
	"username": "user456",
	"firstName":"John",
	"lastName":"Smith",
	"email":"johnsmith@gmail.com",
	"password":"js1234",
	"mobileNumber":"09194567891",
	"isAdmin": false

}


course {

	"id":"course2",
	"name": "Math102",
	"description": "algebra",
	"price":"10000",
	"slots":"20",
	"schedule":"1:00 -3:00 pm",
	"instructor": "Mr.Genos",
	"isActive":

}

enrollment: 
{
	"id":"enrollee02",
	"userId":"user2",
	"userName": "user456",
	"courseId":"course2",
	"courseName": "Math102",
	"isPaid":true,
	"dateEnrolled": "08/25/2022:4:00 pm"
}

// 3. Create a new repo called s27

//4. Initialize your local repo, add and commit with the following message: "Add Activity Code"

// 5. Push and link to boodle (WDC028v1.5b-27 | MongoDB - Data Modeling and Translation).
